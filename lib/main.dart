import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/intl.dart';

main() => runApp(Interfaz());

class Interfaz extends StatefulWidget {
  @override
  State<Interfaz> createState() => _InterfazState();
}

class _InterfazState extends State<Interfaz> {

  @override
  List<Message> messages= [

      Message(
        texto: "Hola hijo",
        fecha: DateTime.now().subtract(Duration(days: 3, minutes: 1)),
        enviadoPorMi: false,
      ),

      Message(
        texto: "Como estas",
        fecha: DateTime.now().subtract(Duration(days: 3,minutes: 1)),
        enviadoPorMi: false,
      ),

      Message(
        texto: "Como te va en la escuela",
        fecha: DateTime.now().subtract(Duration(days: 3,minutes: 1)),
        enviadoPorMi: false,
      ),

      Message(
        texto: "Bien pa, vamos super",
        fecha: DateTime.now().subtract(Duration(days: 3,minutes: 1)),
        enviadoPorMi: true,
      ),

      Message(
        texto: "Sale hijo, echale ganas",
        fecha: DateTime.now().subtract(Duration(days: 3,minutes: 1)),
        enviadoPorMi: false,
      ),

      Message(
        texto: "Hola pa, como estan por alla en la casa",
        fecha: DateTime.now().subtract(Duration(days: 5,minutes: 1)),
        enviadoPorMi: true,
      ),

      Message(
        texto: "Bien hijo, todos bien",
        fecha: DateTime.now().subtract(Duration(days: 5,minutes: 1)),
        enviadoPorMi: false,
      ),

      Message(
          texto: "¿Hoy vienes a cenar a la casa?",
          fecha: DateTime.now().subtract(Duration(minutes: 1)),
          enviadoPorMi: false,
      ),

      Message(
          texto: "Sí, claro!",
          fecha: DateTime.now().subtract(Duration(minutes: 1)),
          enviadoPorMi: true,
    ),

    Message(
          texto: "Te traes un refresco",
          fecha: DateTime.now().subtract(Duration(minutes: 1)),
          enviadoPorMi: false,
    ),

    Message(
          texto: "Ah ok, ¿De cuál marca?",
          fecha: DateTime.now().subtract(Duration(minutes: 1)),
          enviadoPorMi: true,
    ),

    Message(
          texto: "Traete un jarrito de 2 litros",
          fecha: DateTime.now().subtract(Duration(minutes: 1)),
          enviadoPorMi: false,
    ),

    Message(
      texto: "Sale ¿De qué sabor?",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Fijate si hay de manzana",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Hijole, no, no hay, solamente de guayaba y toronja",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Traete entonces uno de toronja",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Va, ¿Llevo algo más?",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Traete unos tostitos flamin hot",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Va, al rato los veo",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Ok hijo, cuidate",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Gracias ustedes igual",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

  ];

  @override
  Widget build(BuildContext context) => MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
          appBar: AppBar(
            title:
            Row(
              children: [
                CircleAvatar(
                  radius: 25,
                  child: ClipOval(
                    child: Image.network("https://pbs.twimg.com/media/FczHkHhXkAACdPQ?format=jpg&name=large"),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(27.7),
                  child: Column(
                    children: [
                      Text("Papá",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 20),
                      ),
                      Text("En línea",
                        textAlign: TextAlign.left,
                        style: TextStyle(fontSize: 12),
                      ),
                    ],
                  ),
                ),


                ButtonTheme(
                  minWidth: 20,
                  height: 50,
                  child: FloatingActionButton(
                      elevation: 0,
                      highlightElevation: 0,
                      backgroundColor: Color.fromARGB(250, 18, 140, 126),
                      child: Icon(
                          size: 25,
                          Icons.videocam,
                          color: Colors.white,

                      ),
                      onPressed: (){

                  }
                  ),
                ),

                ButtonTheme(
                  minWidth: 20,
                  height: 50,
                  child: FloatingActionButton(
                      elevation: 0,
                      highlightElevation: 0,
                      backgroundColor: Color.fromARGB(250, 18, 140, 126),
                      child: Icon(
                        size: 25,
                        Icons.phone,
                        color: Colors.white,
                      ),
                      onPressed: (){

                      }
                      ),
                ),

                ButtonTheme(
                  minWidth: 20,
                  height: 50,
                  child: FloatingActionButton(
                      elevation: 0,
                      highlightElevation: 0,
                      backgroundColor: Color.fromARGB(250, 18, 140, 126),
                      child: Icon(
                        size: 25,
                        Icons.more_vert,
                        color: Colors.white,
                      ),
                      onPressed: (){

                      }
                  ),
                )
              ],
            ),
            backgroundColor: Color.fromARGB(250, 18, 140, 126),
            leading:

            MaterialButton(
              elevation: 0,
              highlightElevation: 0,
              child: Icon(
                  Icons.arrow_back,
                  size: 25,
                  color: Colors.white,
              ),
              onPressed: () {

              },
            ),
          ),

          body:
          Container(
            width: 6000,
            height: 6000,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("img/fondo_wt.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
                children: [
                  Expanded(child: GroupedListView<Message, DateTime>(
                          padding: const EdgeInsets.all(8),
                          reverse: true,
                          order: GroupedListOrder.DESC,
                          useStickyGroupSeparators: true,
                          floatingHeader: true,
                          elements: messages,
                          groupBy: (message) => DateTime(
                            message.fecha.day,
                            message.fecha.month,
                            message.fecha.day,
                          ),
                          groupHeaderBuilder: (Message message) => SizedBox(
                            height: 40,
                            child: Center(
                              child: Card(
                                color: Colors.white38,
                                child: Padding(
                                  padding: const EdgeInsets.all(8),
                                  child: Text(
                                      DateFormat.yMMMd().format(message.fecha),
                                      style: const TextStyle(color: Colors.white),
                                  ),
                                )
                              ),
                            ),
                          ),
                          itemBuilder: (context, Message message) => Align(
                            alignment: message.enviadoPorMi
                              ? Alignment.centerRight
                              : Alignment.centerLeft,
                            child: Card(
                              elevation: 8,
                              child: Padding(
                                padding: const EdgeInsets.all(12),
                                child: Text(message.texto),
                              ),
                            ),
                          ),
                    ),
                  ),
                  Container(
                    color: Colors.grey.shade300,
                    padding: EdgeInsets.symmetric(horizontal: 8),
                    height: 55,
                    child: Row(
                      children: <Widget>[

                        IconButton(
                            onPressed: (){

                        },
                            icon: Icon(Icons.emoji_emotions_outlined),
                            iconSize: 25,
                            color: Colors.black45,
                        ),

                        Expanded(
                          child: TextField(
                              decoration: const InputDecoration(
                              contentPadding: EdgeInsets.all(12),
                              hintText: 'Mensaje'
                            ),
                            onSubmitted: (text){
                              final message = Message(
                                texto: text,
                                fecha: DateTime.now(),
                                enviadoPorMi: true,
                              );
                              setState(() {
                                messages.add(message);
                              });
                            },
                          ),
                        ),

                        IconButton(
                          onPressed: (){

                          },
                          icon: Icon(Icons.attach_file_outlined),
                          iconSize: 25,
                          color: Colors.black45,
                        ),

                        IconButton(
                          onPressed: (){

                          },
                          icon: Icon(Icons.camera_alt_outlined),
                          iconSize: 25,
                          color: Colors.black45,
                        ),

                        ButtonTheme(
                          minWidth: 20,
                          height: 50,
                          child: FloatingActionButton(
                              elevation: 0,
                              highlightElevation: 0,
                              backgroundColor: Color.fromARGB(250, 18, 140, 126),
                              child: Icon(
                                size: 25,
                                Icons.send,
                                color: Colors.white,
                              ),
                              onPressed: (){

                              }
                          ),
                        )

                      ],
                    ),
                  )
                ],
            ),
          ),
      )
  );
}

class Message {
  final String texto;
  final DateTime fecha;
  final bool enviadoPorMi;

  const Message({
    required this.texto,
    required this.fecha,
    required this.enviadoPorMi,
});
}
